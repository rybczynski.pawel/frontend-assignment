import moment from "moment";
import "regenerator-runtime/runtime";

import "./style.scss";

/*
 * At start, I added webpack loaders and configure HTML webpack plugin to generate an HTML5 file. 
 * I added also .babelrc config file with preset to transpile code for ES2015+. 
 * Styling is in separate scss file. The static HTML structure is in HTML file, the dynamic list is created in the script. 
 * Application is responsive, main functionalities ( displaying articles by categories, sorting) are included. 

 * There are many things that can be done to improve that app, both styling, and logic. 
 * The app style is not the best.  Media queries rules can be changed to avoid "!important" rules, also some HTML elements can be changed. 
 * In script should be added some more statements to avoid bugs. The moment.js could be replaced to reduce bundle size.
 */

const sportsCheckbox = document.getElementById("sports-checkbox");
sportsCheckbox.addEventListener("click", handleSportArticles);
sportsCheckbox.checked = false;

const fashionCheckbox = document.getElementById("fashion-checkbox");
fashionCheckbox.addEventListener("click", handleFashionArticles);
fashionCheckbox.checked = false;

const articlesList = document.getElementById("articles-list");
articlesList.style.display = "none";

const mainContentMsgContainer = document.getElementById(
  "main-content-msg-container"
);

const sortContainer = document.getElementById("sort-container");
sortContainer.style.display = "none";

const arrowUp = document.getElementById("arrow-up");
arrowUp.addEventListener("click", () => sortByDate(0));

const arrowDown = document.getElementById("arrow-down");
arrowDown.addEventListener("click", () => sortByDate(1));

let articlesArray = [];

const MONTHS = {
  januar: 1,
  februar: 2,
  mars: 3,
  april: 4,
  mai: 5,
  juni: 6,
  juli: 7,
  august: 8,
  september: 9,
  oktober: 10,
  november: 11,
  desember: 12
};
Object.freeze(MONTHS);

async function handleSportArticles() {
  if (sportsCheckbox.checked) {
    const result = await fetchData(`http://localhost:6010/articles/sports`);

    result && Array.isArray(result.articles)
      ? await addListItems(result.articles)
      : alert("Wrong data format");
  } else {
    await removeListItems("sport");
  }
}

async function handleFashionArticles() {
  if (fashionCheckbox.checked) {
    const result = await fetchData(`http://localhost:6010/articles/fashion`);

    result && Array.isArray(result.articles)
      ? await addListItems(result.articles)
      : alert("Wrong data format");
  } else {
    await removeListItems("fashion");
  }
}

const fetchData = async url => {
  let result = null;
  for (let i = 0; i < 5; i++) {
    let response = await fetch(url);
    if (response.status == 200) {
      let data = await response.json();
      result = data;
      break;
    }
  }
  return result;
};

const addListItems = articles => {
  if (articles.length > 0) {
    articles.forEach(item => {
      const dateWithChangedFormat = changeDateFormat(item.date);
      const listItem = document.createElement("li");
      listItem.id = item.id;
      listItem.timestamp = parseInt(dateWithChangedFormat.timestamp, 10);

      const container = document.createElement("div");
      container.className = "list-item-container";

      const imageContainer = document.createElement("div");
      imageContainer.className = "image-container";
      const image = document.createElement("img");

      if (item.image === "") {
        image.src = "http://placehold.it/200x100";
      } else {
        image.src = item.image;
      }

      imageContainer.appendChild(image);

      const textContainer = document.createElement("div");
      textContainer.className = "text-container";
      const topInfo = document.createElement("div");
      topInfo.className = "top-info";

      const title = document.createElement("span");
      title.className = "item-title";
      title.innerHTML = item.title;

      const data = document.createElement("span");
      data.className = "item-date";
      data.innerHTML = dateWithChangedFormat.formattedDate;
      item.date = dateWithChangedFormat.formattedDate;

      topInfo.appendChild(title);
      topInfo.appendChild(data);

      const preambleContainer = document.createElement("div");
      const preamble = document.createElement("span");
      preambleContainer.className = "preamble-container";
      preamble.className = "preamble";
      preamble.innerHTML = item.preamble;
      preambleContainer.appendChild(preamble);

      textContainer.appendChild(topInfo);
      textContainer.appendChild(preambleContainer);

      container.appendChild(imageContainer);
      container.appendChild(textContainer);

      listItem.appendChild(container);
      articlesList.appendChild(listItem);
    });
    articlesArray.push(...articles);
    sortContainer.style.display = "block";
    articlesList.style.display = "block";
    mainContentMsgContainer.style.display = "none";
  }
};

const changeDateFormat = date => {
  const stringArray = date.split(" ");
  const day = stringArray[0].substring(0, stringArray[0].indexOf("."));
  const month = stringArray[1];
  const shortMonth = month
    .replace(month[0], month[0].toUpperCase())
    .substring(0, 3);
  const year = stringArray[2];
  const formattedTime = moment(`${day} ${MONTHS[month]} ${year}`, "DD M YYYY");
  const timestamp = moment(formattedTime._d).format("X");

  return { formattedDate: `${day}. ${shortMonth} ${year}`, timestamp };
};

const removeListItems = category => {
  articlesArray = articlesArray.filter(element => {
    if (element.category === category) {
      const listItemToRemove = document.getElementById(element.id);
      listItemToRemove.parentNode.removeChild(listItemToRemove);
      return false;
    }
    return true;
  });

  if (articlesArray.length === 0) {
    articlesList.style.display = "none";
    mainContentMsgContainer.style.display = "flex";
    sortContainer.style.display = "none";
  }
};

const sortByDate = type => {
  function compareDatesAsc(a, b) {
    return a.timestamp - b.timestamp;
  }

  function compareDatesDesc(a, b) {
    return b.timestamp - a.timestamp;
  }

  const compareType = type === 0 ? compareDatesAsc : compareDatesDesc;

  const sortedArticles = [];

  for (let i = articlesList.childNodes.length; i--; ) {
    if (articlesList.childNodes[i].nodeName === "LI")
      sortedArticles.push(articlesList.childNodes[i]);
  }

  sortedArticles.sort(compareType);

  articlesList.innerHTML = "";
  for (let i = 0; i < sortedArticles.length; i++) {
    articlesList.appendChild(sortedArticles[i]);
  }
};
